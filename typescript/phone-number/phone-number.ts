let err = (msg: string) => { throw new Error(msg) }

export function clean(phoneNumber: string) {
  if (phoneNumber.match(/[a-zA-Z]/)) err("Letters not permitted")
  if (phoneNumber.match(/[@:!]/)) err("Punctuations not permitted")
  if (phoneNumber.replaceAll(' ', "").split("").map(Number).filter(Number.isFinite).length > 11) err("More than 11 digits")
  if (phoneNumber.replaceAll(' ', "").split("").map(Number).filter(Number.isFinite).length < 10) err("Incorrect number of digits")

  phoneNumber = phoneNumber.replaceAll(/[+()]/g, "")
  if (phoneNumber.length === 11) {
    if (phoneNumber[0] !== "1") err("11 digits must start with 1")
    return phoneNumber.slice(1)
  }

  let number = phoneNumber.split(/[ -.]/).filter(v => v)
  if (number[0].length === 1) {
    if (number[0] != "1") err("country code must start with a 1")
    number.shift()
  }

  let [area_code, exchange_code, subscriber_number] = number
  if (area_code[0] === "1") err("Area code cannot start with one")
  if (area_code[0] === "0") err("Area code cannot start with zero")
  if (exchange_code[0] === "1") err("Exchange code cannot start with one")
  if (exchange_code[0] === "0") err("Exchange code cannot start with zero")

  return area_code + exchange_code + subscriber_number
}
