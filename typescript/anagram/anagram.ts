export class Anagram {
  constructor(private input: string) { this.input = input.toLowerCase() }

  matches = (...potens: string[]) => potens.filter(poten => poten.toLowerCase() !== this.input && [...poten.toLowerCase()].length === [...this.input].sort().length && [...poten.toLowerCase()].sort().join() === [...this.input].sort().join())
}