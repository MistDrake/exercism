export class Triangle {
  A; B; C
  constructor(...sides: [number, number, number]) {
    this.A = sides[0]
    this.B = sides[1]
    this.C = sides[2]
  }

  get isATriangle() { return ![this.A, this.B, this.C].includes(0) }
  get isABEqual() { return this.A === this.B }
  get isBCEqual() { return this.B === this.C }
  get isCAEqual() { return this.C === this.A }
  get isABCInequality() { return this.A + this.B >= this.C }
  get isBCAInequality() { return this.B + this.C >= this.A }
  get isCABInequality() { return this.C + this.A >= this.B }

  get isEquilateral() {
    if (!this.isATriangle) return false
    return (this.isABEqual && this.isBCEqual && this.isCAEqual)
  }

  get isIsosceles() {
    if (!this.isATriangle) return false
    if (this.isABEqual && this.isABCInequality) return true
    if (this.isBCEqual && this.isBCAInequality) return true
    if (this.isCAEqual && this.isCABInequality) return true
    return false
  }

  get isScalene() {
    if (!this.isATriangle) return false
    if (!this.isABCInequality) return false    
    if (!this.isBCAInequality) return false    
    if (!this.isCABInequality) return false      
    if (!this.isABEqual && !this.isBCEqual && !this.isCAEqual) return true
    return false
  }
}
